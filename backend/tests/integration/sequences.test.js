import request from 'supertest';
import { serverWrapper as app } from './helpers';

describe('GET /api/numbers/factorial', () => {
  it('responds with correct factorial sequences', async () => {
    const response = await request(app()).get('/api/numbers/factorial');

    expect(response.status).toBe(200);
    expect(response.body.result).toBe('1');
  });
});

describe('GET /api/numbers/factorial', () => {
  it('responds with correct factorial sequences', async () => {
    const response = await request(app()).get('/api/numbers/factorial');

    expect(response.status).toBe(200);
    expect(response.body.result).toBe('2');
  });
});
