* Testing course
** Plan
- [0/2] Brief intro.
  - [ ] Course structure
  - [ ] Application we'll test (number sequence generator)
- [0/3] What's testing? Types of testing.
  - [ ] [0/3] Unit testing
    - [ ] What is Unit test? What problems Unit tests solve? Who writes them?
    - [ ] Core principles, structure and patterns (xUnit, Assertions,
      Fixtures, Mocks, Stubs...)
    - [ ] Examples
    - [ ] Code challenge
    - [ ] What makes a good unit test? (Code coverage, adv.: mutation testing)
  - [ ] [0/3] Integration testing
    - [ ] What is Integration tests? What problems Integration tests
      solve? Who writes them?
    - [ ] Unit testing vs. Integration testing
    - [ ] When to write integration tests?
  - [ ] [0/3] Functional testing
    - [ ] What is functional test? What problem functional tests
      solve? Who writes them?
    - [ ] Browser-testing tooling.
    - [ ] When to write functional tests?
- [ ] Metodologies: TDD, BDD, ATDD


** Introduction

*** Course structure

During this course we will cover different approaches to testing,
and will discuss advantages and disadvantages of each.

Every subject will be backed up with code examples and we will
also have some time for discussion.

The course is divided into four parts. Three of them is dedicated to
various types of testing and one is for software development
metodologies.


*** Application we'll test

During our course we will use a simple Web Application as
reference.

Application generates fibonacci and factorial number sequences.

**** Tech stack

***** Backend:
- NodeJS
- Koa2

***** Frontend:
- React

***** Testing tools:
- [[https://jestjs.io/][Jest]] as test running & assertions library
- =SOMETHING FOR BROWSER TESTING=

** What's testing? Types of testing.

Testing is a process of investigating & providing information about
quality of the item under test.

Item examples:
- function or method (who needs information about quality of
  function? What's quality of function?)
- class or module
- API endpoint (who needs information about quality of API endpoint?)
- browser page (who needs information about quality of browser page?)

What types of testing are there? =Ask audience.=

There are lots of gradations out there, but we will choose the most
simple and convenient one. This scale grades from low-level to
high-level tests. =NICE GRAPH W/ LEVEL / RESPONSIBILITY HERE=

*** Unit testing

**** What is Unit test? What problems Unit tests solve? Who writes them?

#+BEGIN_QUOTE
“Unit tests test individual units (modules, functions, classes) in
isolation from the rest of the program.”

--- Kent Beck (the author of sUnit, first unit-testing framework)
#+END_QUOTE

From the definition, Unit tests don't contribute to the quality of
program. Why do we need them?
=testing unit interface & expected input / output. Bad-designed units
are hard to test (due to coupling). Refactoring. .=.

Unit tests are not about program quality, but code quality. Good code
easier to maintain, extend, refactor, understand.

Good code is written by good programmers.

**** Core principles, structure and patterns

From the definition, we need to test units in isolation from the rest
of the program. How do we achieve isolation?




*** Integration testing
