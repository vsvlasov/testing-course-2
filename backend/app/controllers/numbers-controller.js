import fibonacci from '../services/numbers/fibonacci.js';
import factorial from '../services/numbers/factorial.js';

let fibonacciGenerator = fibonacci();
let factorialGenerator = factorial();

export default {
  async getNumber(ctx, next) {
    const { params: { type } } = ctx;

    await next();

    let generator;

    switch (type) {
    case 'factorial':
      generator = factorialGenerator;
      break;
    case 'fibonacci':
      generator = fibonacciGenerator;
      break;
    };

    if (generator) {
      ctx.body = { result: generator.next().value };
    }
  },

  async reset(ctx, next) {
    const { params: { type } } = ctx;

    await next();

    let result = null;

    switch (type) {
    case 'factorial':
      factorialGenerator = factorial();
      break;
    case 'fibonacci':
      fibonacciGenerator = fibonacci();
      break;
    case 'all':
      factorialGenerator = factorial();
      fibonacciGenerator = fibonacci();
    };

    ctx.body = 'Ok';
  },
};
