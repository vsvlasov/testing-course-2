import Koa from 'koa';
import views from 'koa-views';
import devProxy from 'koa-proxy';
import statics from 'koa-static';

import router from './router';

const app = new Koa();

if (process.env.NODE_ENV === 'PRODUCTION') {
  console.log('yay');
  app.use(statics(`${__dirname}/../../frontend/build/static`));
} else {
  app.use(devProxy({
    host: 'http://localhost:3000',
    match: /^(?!\/api).*$/,
  }));
}

app.use(router.routes());

export default app;
