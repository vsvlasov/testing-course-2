import BN from 'bn.js';

/**
* Generate a sequence of factorial numbers
*
* @yields {string}
*/
export default function* factorial() {
  let [prev, value] = [new BN(1), new BN(1)];

  while(true) {
    yield prev.toString(10);

    value = value.add(new BN(1));
    prev = value.mul(prev);
  }
}
